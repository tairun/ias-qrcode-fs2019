![https://bitbucket.org/tairun/ias-qrcode-fs2019](pictures/url-repo.png)


# QR-Code chat #
Allows a user to chat with another client via QR-Code messages. It works by pointing the webcam of you PC towards the screen of you chat partner. This work is meant as proof-of-concept, hence the project contains a benchmarking tool to compare data transmission rates for different combination of parameters. Tests we conducted with *Microsoft Surface Pro 5* computers and *Macbook Pro's*. The specs for monitors and webcams are roughly:

| Model | Resolution | Frame/Refresh Rate |
|---|---|---|
| **Microsoft Surface** |||
| Display | 2736 x 1824 | 60Hz |
| Webcam (front) | 5MP (1080p)| 60fps |
| **Macbook Pro** |||
| Webcam (front) | ? | ? |
| Display | ? | ? |

The best data transfer rate we achieved, was a whooping 1,200 *bytes/s*. This was achieved using version 10 QR-Codes with error correction level set to `HIGH`. For version 10 QR-Codes, it was enough to leave the image just 0.1 *seconds* on the screen. If we assume frame perfect transmission (display and cam in sync, and decoding is done also in between frames) for v10H codes (174 *bytes*), the maximum transfer speed is 10,440 *bytes/s*. Which is 1150% of what we achieved in out tests.

## Features ##
This QR-Code chat consists of two classes: the Encoder and the Decoder class. The **Encoder** class takes input from `stdin` and transforms the string into QR-Codes. If the message is too long for one QR-Image, the message is split automatically into multiple images. The version (size) and error correction level can be specified. The **Decoder**" class accesses the webcam and decodes the images in sequence and puts the messages back together if needed.

### Coming soon ... ### 
 - Automatic retransmission of packages
 - Slow-start and TAHOE (like TCP)
 - File transfer (with binary mode)
 - Concurrency
 
## Setup ##
We assume you use Python ***3.6 or newer.*** The project is dependant on the following libraries/modules/frameworks:  
- OpenCV  
- PyZBar  
- Numpy  
- QRCode  
- Pillow  

Most of the dependencies can be installed with pip. We have created a `Pipfile` and a `Pipfile.lock`, which you can use to install all the required dependencies. Under Linux however , **OpenCV** should be installed through your OS's packet manager or a native installer ([https://opencv.org/releases/](https://opencv.org/releases)). Additionally on Linux you need to install `libzbar0`.

```
## Install system level dependencies
$ sudo apt install python3-opencv libzbar0 libzbar-dev
## Install python dependencies
$ pip3 install --user pipenv
$ pipenv install
$ pipenv shell
## Once you're done with the virtual environtment, you need to quit out of it!
$ exit


```
### Windows, Linux and Mac ###


## Screenshots ##


## Other QR-Code related projects ##
- https://github.com/cuicaihao/Webcam_QR_Detector/blob/master/Lab_02_QR_Bar_Code_Detector_Webcam.ipynb
- https://github.com/dlbeer/quirc/