#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from typing import Tuple, List


def simple_read_data(file: str = "report/plt/results.csv", header: int = 1) -> List[pd.DataFrame]:
    """
    Reads csv file from disk and prepares data.
    :param file: to read from disk, either as absolut or relative path.
    :param header: row to skip.
    :return: time_data: unaltered data read from csv, in case you need it.
    """
    time_data = pd.read_csv(file, delimiter=',', header=header, names=['ver', 'err', 'pkt', 'timing', 'time'])
    timings = time_data['timing'].unique()
    filtered_data = []

    for i, timing in enumerate(timings):
        filtered_data.append(time_data[time_data['timing'] == timing])
        filtered_data[i].name = timing

    return filtered_data


def simple_plot(filtered_data: List[pd.DataFrame], show: bool = False) -> plt.Figure:
    """
    Make plot of filtered data. Plot QR-Version against measured time.
    :param filtered_data: list with Pandas DataFrames separated by timings.
    :param show: weather to show the plot or not.
    :return: the matplotlib figure with the plot attached.
    """
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.set_title("Übertragungszeit in Abhängigkeit der QR-Version")
    ax1.set_xlabel('QR-Code Version')
    ax1.set_ylabel('Zeit [ms]')
    ax1.yaxis.set_minor_locator(plt.MultipleLocator(500))

    for t_data in filtered_data:
        ax1.plot(t_data['ver'], t_data['time']*1000, marker='o', linestyle='dashed', label=t_data.name)
    ax1.legend(title='Timings [ms]')
    plt.grid()
    plt.show() if show else None
    return fig


def read_data(file: str = "report/plt/results.csv", header: int = 1) -> List[pd.DataFrame]:
    time_data = pd.read_csv(file, delimiter=',', header=header, names=['ver', 'err', 'pkt', 'timing', 'time'])
    versions = time_data['ver'].unique()
    timings = time_data['timing'].unique()

    filtered_data = []

    for i, timing in enumerate(timings):
        columns = ['ver', 'err', 'pkt', 'mean', 'error']
        temp_df = pd.DataFrame(columns=columns)
        df1 = time_data[time_data['timing'] == timing]
        for version in versions:
            df2 = df1[df1['ver'] == version]

            mean = df2['time'].mean()
            yerror = df2['time'].std()
            row = {'ver': df2.iat[0, 0], 'err': df2.iat[0, 1], 'pkt': df2.iat[0, 2], 'mean': mean, 'error': yerror}
            temp_df = temp_df.append(row, ignore_index=True)

        filtered_data.append(temp_df)
        filtered_data[i].name = timing

    return filtered_data


def plot(filtered_data: List[pd.DataFrame], show: bool = False, save: bool = True) -> List[plt.Figure]:
    """
    Make three plots of filtered data.
    Figure1: QR-Version against mean transmission time for all timings.
    Figure2: QR-Version against numbers of packets.
    Figure3: QR-Version against standard deviation for all timings.
    :param filtered_data: list with Pandas DataFrames separated by timings.
    :param show: weather to show the plot or not.
    :param save: weather to save the plot as image or not.
    :return: the matplotlib figures with the plots attached as a list.
    """
    # Figure1: QR-Version against mean transmission time for all timings
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.set_title("Übertragungszeit in Abhängigkeit der QR-Version")
    ax1.set_xlabel('QR-Code Version')
    ax1.set_ylabel('Ø-Zeit [ms]')
    ax1.yaxis.set_minor_locator(plt.MultipleLocator(500))

    for t_data in filtered_data:
        ax1.plot(t_data['ver'], t_data['mean']*1000, marker='o', linestyle='dashed', label=t_data.name)
    ax1.legend(title='Timings [ms]')
    plt.grid()
    plt.show() if show else None
    fig1.savefig('report/img/plot1.jpg') if save else None

    # Figure2: QR-Version against numbers of packets
    fig2 = plt.figure()
    fig2ax1 = fig2.add_subplot(111)
    for t_data in filtered_data:
        fig2ax1.plot(t_data['ver'], t_data['pkt'])

    fig2ax1.set_title("Anzahl Pakete in Abhängigkeit der QR-Version")
    fig2ax1.set_xlabel('QR-Code Version')
    fig2ax1.set_ylabel('#-Pakete')
    plt.grid()
    plt.show() if show else None
    fig2.savefig('report/img/plot2.jpg') if save else None

    fig3 = plt.figure()
    fig3ax1 = fig3.add_subplot(221)
    fig3ax2 = fig3.add_subplot(222)
    fig3ax3 = fig3.add_subplot(223)
    fig3ax4 = fig3.add_subplot(224)

    # Figure3: QR-Version against standard deviation for all timings
    fig3.suptitle("Std.-Abweichung der Übertragungszeit")
    fig3ax1.bar(filtered_data[0]['ver'], filtered_data[0]['error'], color='tab:blue', label='50 ms')
    fig3ax2.bar(filtered_data[1]['ver'], filtered_data[1]['error'], color='tab:orange', label='25 ms')
    fig3ax3.bar(filtered_data[2]['ver'], filtered_data[2]['error'], color='tab:green', label='10 ms')
    fig3ax4.bar(filtered_data[3]['ver'], filtered_data[3]['error'], color='tab:red', label='5 ms')
    fig3ax1.legend(loc='upper right')
    fig3ax2.legend(loc='upper right')
    fig3ax3.legend(loc='upper right')
    fig3ax4.legend(loc='upper right')
    fig3ax1.set_xticks(np.arange(2, 11, 1))
    fig3ax2.set_xticks(np.arange(2, 11, 1))
    fig3ax3.set_xticks(np.arange(2, 11, 1))
    fig3ax4.set_xticks(np.arange(2, 11, 1))
    fig3ax1.set_yticks(np.arange(0, 1.2, 0.2))
    fig3ax2.set_yticks(np.arange(0, 1.2, 0.2))
    fig3ax3.set_yticks(np.arange(0, 1.2, 0.2))
    fig3ax4.set_yticks(np.arange(0, 1.2, 0.2))
    fig3ax1.set_ylabel('Std.-Abweichung [ms]')
    fig3ax3.set_xlabel('QR-Code Version')
    fig3ax3.set_ylabel('Std.-Abweichung [ms]')
    fig3ax4.set_xlabel('QR-Code Version')

    plt.show() if show else None
    fig3.savefig('report/img/plot3.jpg') if save else None

    return [fig1, fig2, fig3]


if __name__ == "__main__":
    #filtered_data = simple_read_data()
    #fig1 = simple_plot(filtered_data, show=True)
    #fig1.savefig('report/img/plot1.jpg')

    filtered_data2 = read_data(file='report/plt/results2.csv')
    plot(filtered_data2, show=True, save=True)
