#!/usr/bin/env python3

import argparse
from QREncode import EncoderBenchmark
from QRDecode import DecoderBenchmark

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Module to automate the benchmarking of the \'QREncode\' and \'QRDecode\' class.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-d', '--decoder', help='Select decoder benchmark. Do this one first!', action='store_true')
    group.add_argument('-e', '--encoder', help='Select encoder benchmark.', action='store_true')
    args = parser.parse_args()

    if args.encoder:
        enc_bench = EncoderBenchmark(versions=[x for x in range(10, 1, -1)], error_corrections=['H'], cycles=5)
        enc_bench.run()
    if args.decoder:
        dec_bench = DecoderBenchmark()
        dec_bench.run()
