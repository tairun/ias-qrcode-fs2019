#!/usr/bin/env python3

from qrcode.constants import ERROR_CORRECT_H, ERROR_CORRECT_L, ERROR_CORRECT_M, ERROR_CORRECT_Q


class Constants:
    error_levels = {
        'L': ERROR_CORRECT_L,
        'M': ERROR_CORRECT_M,
        'Q': ERROR_CORRECT_Q,
        'H': ERROR_CORRECT_H
    }

    qr_capacity = {
        1: {
            'L': {
                "alpha": 25,
                "bin": 17
            },
            'M': {
                "alpha": 20,
                "bin": 14
            },
            'Q': {
                "alpha": 16,
                "bin": 11
            },
            'H': {
                "alpha": 10,
                "bin": 7
            }
        },
        2: {
            'L': {
                "alpha": 47,
                "bin": 32
            },
            'M': {
                "alpha": 38,
                "bin": 26
            },
            'Q': {
                "alpha": 29,
                "bin": 20
            },
            'H': {
                "alpha": 20,
                "bin": 14
            }
        },
        3: {
            'L': {
                "alpha": 77,
                "bin": 53
            },
            'M': {
                "alpha": 61,
                "bin": 42
            },
            'Q': {
                "alpha": 47,
                "bin": 32
            },
            'H': {
                "alpha": 35,
                "bin": 24
            }
        },
        4: {
            'L': {
                "alpha": 114,
                "bin": 78
            },
            'M': {
                "alpha": 90,
                "bin": 62
            },
            'Q': {
                "alpha": 67,
                "bin": 46
            },
            'H': {
                "alpha": 50,
                "bin": 34
            }
        },
        5: {
            'L': {
                "alpha": 154,
                "bin": 106
            },
            'M': {
                "alpha": 122,
                "bin": 84
            },
            'Q': {
                "alpha": 87,
                "bin": 60
            },
            'H': {
                "alpha": 64,
                "bin": 44
            }
        },
        6: {
            'L': {
                "alpha": 195,
                "bin": 134
            },
            'M': {
                "alpha": 154,
                "bin": 106
            },
            'Q': {
                "alpha": 108,
                "bin": 74
            },
            'H': {
                "alpha": 84,
                "bin": 58
            }
        },
        7: {
            'L': {
                "alpha": 224,
                "bin": 154
            },
            'M': {
                "alpha": 178,
                "bin": 122
            },
            'Q': {
                "alpha": 125,
                "bin": 86
            },
            'H': {
                "alpha": 93,
                "bin": 64
            }
        },
        8: {
            'L': {
                "alpha": 279,
                "bin": 192
            },
            'M': {
                "alpha": 221,
                "bin": 152
            },
            'Q': {
                "alpha": 157,
                "bin": 108
            },
            'H': {
                "alpha": 122,
                "bin": 84
            }
        },
        9: {
            'L': {
                "alpha": 335,
                "bin": 230
            },
            'M': {
                "alpha": 262,
                "bin": 180
            },
            'Q': {
                "alpha": 189,
                "bin": 130
            },
            'H': {
                "alpha": 143,
                "bin": 98
            }
        },
        10: {
            'L': {
                "alpha": 395,
                "bin": 271
            },
            'M': {
                "alpha": 311,
                "bin": 213
            },
            'Q': {
                "alpha": 221,
                "bin": 151
            },
            'H': {
                "alpha": 174,
                "bin": 119
            }
        },
        11: {
            'L': {
                "alpha": 468,
                "bin": 321
            },
            'M': {
                "alpha": 366,
                "bin": 251
            },
            'Q': {
                "alpha": 259,
                "bin": 177
            },
            'H': {
                "alpha": 200,
                "bin": 137
            }
        },
        12: {
            'L': {
                "alpha": 535,
                "bin": 367
            },
            'M': {
                "alpha": 419,
                "bin": 287
            },
            'Q': {
                "alpha": 296,
                "bin": 203
            },
            'H': {
                "alpha": 227,
                "bin": 155
            }
        },
        13: {
            'L': {
                "alpha": 619,
                "bin": 425
            },
            'M': {
                "alpha": 483,
                "bin": 331
            },
            'Q': {
                "alpha": 352,
                "bin": 241
            },
            'H': {
                "alpha": 259,
                "bin": 177
            }
        },
        14: {
            'L': {
                "alpha": 667,
                "bin": 458
            },
            'M': {
                "alpha": 528,
                "bin": 362
            },
            'Q': {
                "alpha": 376,
                "bin": 258
            },
            'H': {
                "alpha": 283,
                "bin": 194
            }
        },
        15: {
            'L': {
                "alpha": 758,
                "bin": 520
            },
            'M': {
                "alpha": 600,
                "bin": 412
            },
            'Q': {
                "alpha": 426,
                "bin": 292
            },
            'H': {
                "alpha": 321,
                "bin": 220
            }
        },
        16: {
            'L': {
                "alpha": 854,
                "bin": 586
            },
            'M': {
                "alpha": 656,
                "bin": 450
            },
            'Q': {
                "alpha": 470,
                "bin": 322
            },
            'H': {
                "alpha": 365,
                "bin": 250
            }
        },
        17: {
            'L': {
                "alpha": 938,
                "bin": 644
            },
            'M': {
                "alpha": 734,
                "bin": 504
            },
            'Q': {
                "alpha": 531,
                "bin": 364
            },
            'H': {
                "alpha": 408,
                "bin": 280
            }
        },
        18: {
            'L': {
                "alpha": 1046,
                "bin": 718
            },
            'M': {
                "alpha": 816,
                "bin": 560
            },
            'Q': {
                "alpha": 574,
                "bin": 394
            },
            'H': {
                "alpha": 452,
                "bin": 310
            }
        },
        19: {
            'L': {
                "alpha": 1153,
                "bin": 792
            },
            'M': {
                "alpha": 909,
                "bin": 624
            },
            'Q': {
                "alpha": 644,
                "bin": 442
            },
            'H': {
                "alpha": 493,
                "bin": 338
            }
        },
        20: {
            'L': {
                "alpha": 1249,
                "bin": 858
            },
            'M': {
                "alpha": 970,
                "bin": 666
            },
            'Q': {
                "alpha": 702,
                "bin": 482
            },
            'H': {
                "alpha": 557,
                "bin": 382
            }
        }
    }
