#!/usr/bin/env python3

import time
import qrcode
import cv2 as cv
import numpy as np
from QRConstants import Constants
from typing import List


class Encoder:
    """
    Encode a message in QR-Codes. Automatically split in smaller
    parts so they fit into a single code.
    """

    def __init__(self, version: int = 10, error_correction: str = 'H', timing: float = 0.25, encoding: str = "bin",
                 benchmark: bool = False, debug: bool = False):
        self.version = version
        self.timing = timing
        self.encoding = encoding
        self.benchmark = benchmark
        self.debug = debug

        self.error_correction = Constants.error_levels[error_correction]
        self.maxlength = Constants.qr_capacity[version][error_correction][encoding]
        self.box = Encoder.get_box_size(self.version)

    def set(self, version: int = None, error_correction: str = None, timing: float = None,
            encoding: str = None) -> None:
        """
        Sets the individual attributes of the encoder object.
        :param version:
        :param error_correction:
        :param timing:
        :param encoding:
        :return: None
        """
        self.version = version or self.version
        self.timing = timing or self.timing
        self.encoding = encoding or self.encoding

        self.error_correction = Constants.error_levels[error_correction] or self.error_correction
        self.maxlength = Constants.qr_capacity[self.version][self.error_correction][self.encoding]
        self.box = Encoder.get_box_size(self.version)

    @staticmethod
    def get_box_size(version: int) -> int:
        if version <= 10:
            return int((-16 * version) / 9 + 241 / 9)
        else:
            # versions 11 - 20
            return int((-1 * version) / 3 + 41 / 3)

    def split_msg(self, msg: str) -> List[str]:
        """
        Splits the message into a list of smaller messages so they fit into the specified QR-Format.
        The method prepends a 'header' with the format `##/## ` (where # represents an integer) to show the sequence of
        the messages.
        :param msg: is the message to be split
        :return: a list of messages parts.
        """
        messages = [msg[i:i + (self.maxlength - 6)] for i in range(0, len(msg), (self.maxlength - 6))]
        for j in range(len(messages)):
            messages[j] = str(j) + '/' + str(len(messages) - 1) + '/' + messages[j]
            # print(convert[j]) if self.debug else None
        print("number of packets:", len(messages)) if self.debug else None

        return messages

    def create_qr(self, msg: str):
        """
        Creates a QR-Code-Image based on input message and version number.
        :param msg: to be put into QR-Code.
        :return: the generated QR-Code-Image.
        """
        qr = qrcode.QRCode(
            version=self.version,
            error_correction=self.error_correction,
            box_size=self.box,
            border=4,
        )

        qr.add_data(msg)
        qr.make(fit=False)
        # TODO: Kepp image in memory and convert with numpy, so cv2 can display directly.
        #  --> Also write generator function to do this.
        img = qr.make_image()
        img.save('qrMSG.png')
        return img

    def show(self, msg: str) -> None:
        """
        Displays the QR-Code-Image in a CV2 frame.
        :param msg: is the message to be encoded in the QR-Code.
        :return: None
        """
        img = self.create_qr(msg)  # TODO: Pass image directly to show function. Don't save on disk.
        open_cv_image = cv.imread("qrMSG.png", 0)
        #open_cv_image = cv.cvtColor(np.array(img), cv.COLOR_RGB2BGR)
        cv.imshow('qr-window', open_cv_image)
        cv.waitKey(2)

    def run(self):
        """
        Run the encoder in an endless loop. Accepts messages from `stdin`.
        :return: None
        """
        print("[Encoder] Starting chat...")

        cv.namedWindow('qr-window', cv.WINDOW_AUTOSIZE)
        cv.moveWindow('qr-window', 0, 0)

        try:
            while True:
                message = input("Your message: ")

                if message == "quit":
                    return
                else:
                    messages = self.split_msg(message)
                    for msg in messages:
                        self.show(msg)
                        time.sleep(self.timing)
                    if self.benchmark:
                        return
        except KeyboardInterrupt as e:
            print("\n\nQuitting application... Bye!", e)
        finally:
            cv.destroyAllWindows()
            return


class EncoderBenchmark:
    """
    Benchmark class for Encoder object.
    """
    def __init__(self, versions: List[int] = None, error_corrections=None, msg: str = None,
                 timings: List[float] = None, cycles: int = 1):
        self.encoder = Encoder(benchmark=True)
        # 1000 characters
        # self.defualt_msg = """Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor 
        # invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores 
        # et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor 
        # sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam 
        # erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea 
        # takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam 
        # nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et
        #  justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. 
        #  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu f"""

        # 600 characters
        self.defualt_msg = """Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor 
        invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo 
        dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem 
        ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore 
        magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita 
        kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ip """

        self.versions = versions or range(20, 1, (-1))
        self.error_corrections = error_corrections or ['L', 'M', 'Q', 'H']
        self.msg = msg or self.defualt_msg
        self.timings = timings or [0.05, 0.025, 0.010, 0.005]
        self.cycles = cycles

    def run(self):
        """
        Runs the benchmark with the selected options.
        :return: None
        """
        print(self.versions)
        for version in self.versions:
            self.encoder.set(version=version)
            for level in self.error_corrections:
                self.encoder.set(error_correction=level)
                for timing in self.timings:
                    self.encoder.set(timing=timing)
                    for i in range(self.cycles):
                        print(version, level, timing, i)
                        self.encoder.run()


if __name__ == '__main__':
    # Run the encoder in an infinite loop from the console.
    encoder = Encoder()
    encoder.run()
