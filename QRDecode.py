#!/usr/bin/env python3

import pyzbar.pyzbar as pyzbar
import cv2 as cv
import numpy as np
import time
import random as rd
from typing import Union


class Decoder:
    """
    Decodes QR-Codes captures by the camera and prints them to the terminal.
    """
    def __init__(self, cam_in: int = 0, benchmark: bool = False, debug: bool = False):
        self.cam_in = cam_in
        self.benchmark = benchmark
        self.debug = debug

        self.cap = None
        self.msgList = None
        self.oldMsg = None

    def save(self, decoded_msg) -> Union[float, None]:
        """
        Called for each decoded string from QR-Code. Saves text to msgList array as part of the whole message.
        :return: measured time or None
        :param decoded_msg: Decoded message from QR-Code.
        """
        unformated_msg = decoded_msg.decode().split("/", 2)
        index = int(unformated_msg[0])
        total = int(unformated_msg[1])
        msg = unformated_msg[2]

        starttime = 0.0

        if len(self.msgList) < int(total + 1):
            starttime = time.time()
            self.msgList = [0] * (total + 1)
            self.msgList[index] = msg
        else:
            # does element exist?
            if self.msgList[index] != msg:
                self.msgList[index] = msg
            if index == total:
                if 0 in self.msgList:
                    print("failed...")
                    starttime = time.time()
                    # request
                    return None
                else:
                    endtime = time.time()
                    print("time:", endtime - starttime)
                    print("#packets:", len(self.msgList))
                    self.msgList = [0]
                    return endtime - starttime

    def print_msg(self) -> None:
        """
        Prints the message to 'stdout'.
        :return: None
        """
        s = "".join(self.msgList)

        if rd.randint(0, 2) == 1:
            print("HIM: ", s)
        else:
            print("HER: ", s)

    @staticmethod
    def decode(img):
        """
        Decodes a QR-Code-Image and returns the decoded data as an object.
        :param img: Image with QR-Code to be decoded.
        :return: Object with decoded data.
        """
        decoded_objects = pyzbar.decode(img)  # Find barcode and QR codes
        print(type(decoded_objects))
        return decoded_objects

    def run(self) -> float:
        """
        Run the decoder in an endless loop. Starts the video capture (+ preview) and prints decoded
        messages to `stdout`.
        :return: Returns the timedelta between first and last message and a dict with information
        about the transmission.
        """
        self.oldMsg = "start"
        self.msgList = [0]
        self.cap = cv.VideoCapture(self.cam_in)

        if not self.cap.isOpened():
            print(f"No video input detected on source id: {self.cap}.")
            return 0.0

        self.cap.set(3, 640)
        self.cap.set(4, 480)

        print("[Decoder] Starting chat ...")

        time.sleep(2)  # Allow camera to initialize before doing anything else.
        #font = cv.FONT_HERSHEY_SIMPLEX

        timedelta: float = None

        while self.cap.isOpened():
            ret, frame = self.cap.read()  # Capture frame-by-frame
            img = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)  # Our operations on the frame come here (make it greyscale)
            decoded_objects = self.decode(img)

            for decodedObject in decoded_objects:
                points = decodedObject.polygon

                # If the points do not form a quad, find convex hull
                if len(points) > 4:
                    hull = cv.convexHull(np.array([point for point in points], dtype=np.float32))
                    hull = list(map(tuple, np.squeeze(hull)))
                else:
                    hull = points

                n = len(hull)  # Number of points in the convex hull

                # Draw the convex hull
                for j in range(0, n):
                    cv.line(frame, hull[j], hull[(j + 1) % n], (0, 0, 255), 3)

                if decodedObject.data != self.oldMsg:
                    timedelta = self.save(decodedObject.data)
                    self.oldMsg = decodedObject.data

                #bar_code = str(decodedObject.data)  # TODO: Was macht das hier?

            # cv.putText(frame, barCode, (x, y), font, 1, (0,255,255), 2, cv.LINE_AA)

            cv.imshow('frame', frame)  # Display the resulting frame
            key = cv.waitKey(1)

            if key & 0xFF == ord('q'):
                self.cap.release()
                cv.destroyAllWindows()
                break
            elif key & 0xFF == ord('s'):  # wait for 's' key to save
                cv.imwrite('Capture.png', frame)
            elif key & 0xFF == ord('n'):  # wait for 'n' key to switch to next camera input
                old_cam_in = self.cam_in
                self.cam_in += 1
                self.cap = cv.VideoCapture(self.cam_in)
                if not self.cap.isOpened():
                    print(f"Video source with id:{self.cap} is not available. Reverting to last source.")
                    self.cap = cv.VideoCapture(old_cam_in)
            elif key & 0xFF == ord('p'):  # wait for 'p' key to switch to previous camera input
                old_cam_in = self.cam_in
                self.cam_in -= 1
                self.cap = cv.VideoCapture(self.cam_in)
                if not self.cap.isOpened():
                    print(f"Video source with id:{self.cap} is not available. Reverting to last source.")
                    self.cap = cv.VideoCapture(old_cam_in)

            if self.benchmark:
                return timedelta


class DecoderBenchmark:
    """
    Benchmark class for Decoder object. Will log the speed for all the transmissions.
    Options for graphing with 'matplotlib' and output to csv.
    """
    def __init__(self, csv=False, plot=False):
        self.csv = csv
        self.plot = plot
        self.decoder = Decoder(benchmark=True)

    def run(self):
        """
        Runs the benchmark with the selected options.
        :return: None
        """
        pass


if __name__ == '__main__':
    # Run the decoder in an infinite loop from the console.
    decoder = Decoder()
    decoder.run()
